﻿IdTooltip

LICENSE

This addon is released under the MIT License, see LICENSE.txt for more details.

DESCRIPTION

IdTooltip displays the ID of an item in its tooltip. It works both with the mouse over
tooltip and the persistent tooltip created by clicking a chat linked item.

It was created to help users find item IDs in order to use the [item][/item] feature of the RoR forums.
