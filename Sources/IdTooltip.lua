--[[--------------------------------------------------------------------------
Copyright (c) 2019 Slaynot

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
--]]--------------------------------------------------------------------------

IdTooltip = {}

--[[--------------------------------------------------------------------------
Description:
  Called when the addon is loaded
  Initialize the hooking of Tooltips.CreateItemTooltip
]]----------------------------------------------------------------------------
function IdTooltip.Initialize()

  -- Hooking Tooltips.SetItemTooltipData
  IdTooltip.OriginalSetItemTooltipData = Tooltips.SetItemTooltipData
  Tooltips.SetItemTooltipData = IdTooltip.SetItemTooltipDataHook

end

--[[--------------------------------------------------------------------------
Description:
  Hook of Tooltips.SetItemTooltipData
  Add the item ID to the tooltip text
]]----------------------------------------------------------------------------
function IdTooltip.SetItemTooltipDataHook(tooltipWindow, itemData, extraText, extraTextColor)

  local id_text = L"Item id: " .. itemData.uniqueID
  if extraText then
    extraText = extraText .. L"\n" .. id_text
  else
  	extraText = id_text
  end

  -- Calling original Tooltips.SetItemTooltipData
  return IdTooltip.OriginalSetItemTooltipData(tooltipWindow, itemData, extraText, extraTextColor)

end
